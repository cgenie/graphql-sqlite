let () =
  Printf.printf "I'm a graphql server\n";

  (* Graphql_example.serve () *)

  let dbname = ref "db.sqlite" in

  let _ = Arg.parse [] (fun x ->
    dbname := x
  ) "USAGE" in

  let db = Sqlite3.db_open !dbname in

  Graphql_sqlite.serve db
