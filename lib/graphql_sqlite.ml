open Graphql_lwt
open Sqlite_info

let query_table db (table: Sqlite3_table.t) =
  let fieldnames = List.map (fun (f: Sqlite3_field.t) -> f.name) table.fields in
  let sql = Printf.sprintf "SELECT %s FROM %s;" (String.concat ", " fieldnames) table.name in

  let ret = ref [] in

  let cb row _ =
    ret := List.cons (Array.to_list row) !ret
  in

  let _ = Sqlite3.exec db
       sql
       ~cb:cb
  in

  Printf.printf "Querying table %s" table.name;

  !ret

let field_convert _db idx f =
  let open Schema in
  let open Sqlite3_field in

  match f.typ with
    | INTEGER -> field f.name
                      ~typ:int
                      ~args:Arg.[]
                      ~resolve:(fun (_ctx: unit resolve_info) p ->
         Printf.printf "TODO: get resolve for %s" f.name;
         match (List.nth p idx) with
           | Some i -> Some (int_of_string i)
           | None   -> None
         )
    | TEXT    -> field f.name
                      ~typ:string
                      ~args:Arg.[]
                      ~resolve:(fun (_ctx: unit resolve_info) p -> Printf.printf "TODO: get resolve for %s" f.name; List.nth p idx)
    | BLOB    -> field f.name
                      ~typ:string
                      ~args:Arg.[]
                      ~resolve:(fun (_ctx: unit resolve_info) p -> Printf.printf "TODO: get resolve for %s" f.name; List.nth p idx)
    | REAL    -> field f.name
                      ~typ:float
                      ~args:Arg.[]
                      ~resolve:(fun (_ctx: unit resolve_info) p ->
         Printf.printf "TODO: get resolve for %s" f.name;
         match (List.nth p idx) with
           | Some f -> Some (float_of_string f)
           | None   -> None
         )
    | NUMERIC    -> field f.name
                      ~typ:float
                      ~args:Arg.[]
                      ~resolve:(fun (_ctx: unit resolve_info) p ->
         Printf.printf "TODO: get resolve for %s" f.name;
         match (List.nth p idx) with
           | Some f -> Some (float_of_string f)
           | None   -> None
         )

(*
let relation_convert db (relation: Sqlite3_relation.t) =
  let st, sf = relation.source in
  let dt, df = relation.destination in
  let name = Printf.sprintf "%s_%s_%s_%s" st.name sf.name dt.name df.name in
  field name
    ~typ: 
TODO: how to get the type for this relation?
*)

let make_table_schema db (table: Sqlite3_table.t) (_relations: Sqlite3_relation.t list) =
  let open Schema in

  obj table.name
    ~fields:(fun _ ->
        let fs = List.mapi (field_convert db) table.fields in
        (* let rs = List.mapi (relation_convert db) relations in *)
        let rs = [] in
        List.append fs rs
    )

let make_schema db =
  let open Schema in

  let tables = Sqlite3_table.tables db in

  let ss = List.map (fun table ->
    let table_relations = Sqlite3_relation.single_table_relations db table tables in
    let table_schema = make_table_schema db table table_relations in

    io_field table.name
      ~typ:(non_null (list (non_null table_schema)))
      ~args:Arg.[]
      ~resolve:(fun _ () -> Lwt_result.return (query_table db table))
  ) tables
  in

  schema ss

module Graphql_cohttp_lwt = Graphql_cohttp.Make (Graphql_lwt.Schema) (Cohttp_lwt_unix.IO) (Cohttp_lwt.Body)

let serve db =
  let on_exn = function
    | Unix.Unix_error (error, func, arg) ->
      Logs.warn (fun m ->
          m  "Client connection error %s: %s(%S)"
            (Unix.error_message error) func arg
        )
    | exn -> Logs.err (fun m -> m "Unhandled exception: %a" Fmt.exn exn)
  in
  let callback = Graphql_cohttp_lwt.make_callback (fun _req -> ()) (make_schema db) in
  let server = Cohttp_lwt_unix.Server.make_response_action ~callback () in
  let mode = `TCP (`Port 8081) in
  Cohttp_lwt_unix.Server.create ~on_exn ~mode server
  |> Lwt_main.run
