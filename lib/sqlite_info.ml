open Sqlite3

module Sqlite3_field = struct
  type column_type = INTEGER | TEXT | BLOB | REAL | NUMERIC

  type t = {
    name: string;
    nullable: bool;
    typ: column_type;
  }

  let make name typ_str nullable =
    let typ = match typ_str with
      | "INTEGER" -> INTEGER
      | "TEXT"    -> TEXT
      | "BLOB"    -> BLOB
      | "REAL"    -> REAL
      | "NUMERIC" -> NUMERIC
      | s         -> raise (Failure (Printf.sprintf "Usupported type %s" s))
    in

    {
      name=name;
      nullable=nullable;
      typ=typ;
    }
end

module Sqlite3_table = struct
  type t = {
    name: string;
    fields: Sqlite3_field.t list;
  }

  let table_fields db tablename =
    let ret = ref [] in

    let cb row _ =
      let _ = row |> Array.to_list |> List.map (fun c ->
        match c with
          | Some n -> n
          | None -> "NULL"
      ) |> String.concat " | " |> Printf.printf "%s\n"
      in
      let field = match row with
        | [| _; Some name; Some typ; Some _; _; _ |] -> Sqlite3_field.make name typ true
        | [| _; Some name; Some typ; None; _; _ |] -> Sqlite3_field.make name typ false
        | _ -> raise (Failure "NULL returned")
      in

      ret := List.cons field !ret
    in

    let _ = exec db
      (Printf.sprintf "pragma table_info(%s);" tablename)
      ~cb:cb
    in

    !ret

  let make db tablename =
    {name=tablename; fields=table_fields db tablename}

  let tables db =
    let ret = ref [] in

    let cb row _ =
      match row.(0) with
        | Some name -> ret := List.cons (make db name) !ret
        | None -> raise (Failure "NULL returned")
    in

    let _ = exec db
        "SELECT name FROM sqlite_master WHERE type = 'table' AND name NOT IN ('sqlite_master', 'sqlite_sequence', 'sqlite_stat1');"
        ~cb:cb
    in

    !ret
end

module Sqlite3_relation = struct
  type t = {
    source: Sqlite3_table.t * Sqlite3_field.t;
    destination: Sqlite3_table.t * Sqlite3_field.t;
  }

  let make st sf dt df = {
    source=(st, sf);
    destination=(dt, df)
  }

  let single_table_relations db (table: Sqlite3_table.t) (all_tables: Sqlite3_table.t list) =
    let ret = ref [] in

    let cb row _ =
      let _ = row |> Array.to_list |> List.map (fun c ->
        match c with
          | Some n -> n
          | None -> "NULL"
      ) |> String.concat " | " |> Printf.printf "%s\n"
      in
      let relation = match row with
        | [| _; _; Some destination_table_name; Some source_field_name; Some destination_field_name; _; _; _ |] ->
          let source_field = List.find (fun (f: Sqlite3_field.t) -> f.name = source_field_name) table.fields in
          let destination_table = List.find (fun (t: Sqlite3_table.t) -> t.name = destination_table_name) all_tables in
          let destination_field = List.find (fun (f: Sqlite3_field.t) -> f.name = destination_field_name) destination_table.fields in
          make table source_field destination_table destination_field
        | _ -> raise (Failure "NULL returned")
      in

      ret := List.cons relation !ret
    in

    let _ = exec db
      (Printf.sprintf "pragma foreign_key_list('%s');" table.name)
      ~cb:cb
    in

    !ret
end
