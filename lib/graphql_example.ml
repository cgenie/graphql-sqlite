open Graphql_lwt

type user = {
  id : int;
  name : string;
}
let users = [
  { id = 1; name = "Alice" };
  { id = 2; name = "Bob" }
]

let user =
  let open Schema in
  obj "user"
    ~doc:"Some user"
    ~fields:(fun _ -> [
          field "id"
            ~doc:"Unique id"
            ~typ:(non_null int)
            ~args:Arg.[]
            ~resolve:(fun (_ctx: unit resolve_info) p -> p.id)
          ;
          field "name"
            ~doc:"User name"
            ~typ:(non_null string)
            ~args:Arg.[]
            ~resolve:(fun (_ctx: unit resolve_info) p -> p.name)
          ;
        ])


let user_predicate id_name (u: user) =
    match id_name with
      | None -> true
      | Some (id, name) -> (
          let id_predicate u_ = match id with
          | Some i -> u_.id = i
          | None   -> true
          and name_predicate u_ = match name with
          | Some n -> u_.name = n
          | None   -> true
          in
          (id_predicate u) && (name_predicate u)
      )

let schema =
  let open Schema in

  let user_arg = Arg.(obj "user"
    ~fields:[
      arg "id" ~typ:int;
      arg "name" ~typ:string
    ]
    ~coerce:(fun id name ->
      (id, name)
    )
  ) in

  schema [
    io_field "users"
      ~typ:(non_null (list (non_null user)))
      ~args:Arg.[
        arg "user" ~typ:user_arg
      ]
      ~resolve:(fun _ () id_name -> users |> List.filter (user_predicate id_name) |> Lwt_result.return)
  ]

module Graphql_cohttp_lwt = Graphql_cohttp.Make (Graphql_lwt.Schema) (Cohttp_lwt_unix.IO) (Cohttp_lwt.Body)

let serve () =
  let on_exn = function
    | Unix.Unix_error (error, func, arg) ->
      Logs.warn (fun m ->
          m  "Client connection error %s: %s(%S)"
            (Unix.error_message error) func arg
        )
    | exn -> Logs.err (fun m -> m "Unhandled exception: %a" Fmt.exn exn)
  in
  let callback = Graphql_cohttp_lwt.make_callback (fun _req -> ()) schema in
  let server = Cohttp_lwt_unix.Server.make_response_action ~callback () in
  let mode = `TCP (`Port 8081) in
  Cohttp_lwt_unix.Server.create ~on_exn ~mode server
  |> Lwt_main.run
