#require "sqlite3";;

open Sqlite3;;

let db = db_open "db.sqlite";;

let print_row row =
    let colmap = function
        | Some c -> c
        | None   -> "NULL"
    in
    let rows = Array.map colmap row |> Array.to_list in
    Printf.printf "%s\n" (String.concat ", " rows);;

let results = exec db
    "select name from sqlite_master where type='table'"
     ~cb:(fun row _ -> print_row row)

let schema = Graphql_sqlite.make_schema db;;
